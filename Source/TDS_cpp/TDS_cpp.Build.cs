// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TDS_cpp : ModuleRules
{
	public TDS_cpp(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule", "PhysicsCore", "Slate" });
        PublicIncludePaths.AddRange(new string[]
            {
                 "TDS_cpp",
                 "TDS_cpp/Character",
                 "TDS_cpp/FuncLibrary",
                 "TDS_cpp/Game",
                 "TDS_cpp/Weapons",
                 "TDS_cpp/Weapons/Projectiles",
                 "TDS_cpp/Interface",
                 "TDS_cpp/Structure",
                 "TDS_cpp/StateEffects"

            });
    }
}
