// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_cpp.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_cpp, "TDS_cpp" );

DEFINE_LOG_CATEGORY(LogTDS_cpp)
DEFINE_LOG_CATEGORY(LogTDS_cpp_Net)
 