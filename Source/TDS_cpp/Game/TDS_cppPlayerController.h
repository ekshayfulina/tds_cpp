// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TDS_cppPlayerController.generated.h"

UCLASS()
class ATDS_cppPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATDS_cppPlayerController();
	virtual void OnUnPossess()override;

};