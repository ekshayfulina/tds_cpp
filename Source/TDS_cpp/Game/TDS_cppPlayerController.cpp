// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_cppPlayerController.h"
#include "Engine/World.h"

ATDS_cppPlayerController::ATDS_cppPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void ATDS_cppPlayerController::OnUnPossess()
{
	Super::OnUnPossess();
}
