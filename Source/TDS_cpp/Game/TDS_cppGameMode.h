// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDS_cppGameMode.generated.h"

UCLASS(minimalapi)
class ATDS_cppGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDS_cppGameMode();
	void PlayerCharacterDead();

};



