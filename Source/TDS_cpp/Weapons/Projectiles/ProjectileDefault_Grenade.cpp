// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapons/Projectiles/ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "Interface/TDS_cppIGameActor.h"
#include "DrawDebugHelpers.h"

int32 DebugExplodeShow = 0;
FAutoConsoleVariableRef CVARExplodeShow(
	TEXT("TDS.DebugExplode"),
	DebugExplodeShow,
	TEXT("Draw Debug for Explode"),
	ECVF_Cheat);

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (HasAuthority())
	{
		TimerExploses(DeltaTime);
	}
}

void AProjectileDefault_Grenade::TimerExploses(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplode > TimeToExplode)
		{
			//Explode
			Explode_OnServer();
		}
		else
		{
			TimerToExplode += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!TimerEnabled)
	{
		Explode_OnServer();
	}
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//Init Grenade
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explode_OnServer_Implementation()
{

	if (DebugExplodeShow)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage, 12, FColor::Green, false, 12.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 12, FColor::Red, false, 12.0f);
	}

	TimerEnabled = false;
	if (ProjectileSetting.ExplodeFX)
	{
		SpawnGrenadeFx_Multicast(ProjectileSetting.ExplodeFX);
	}
	if (ProjectileSetting.ExplodeSound)
	{
		SpawnGrenadeSound_Multicast(ProjectileSetting.ExplodeSound);
	}
	
	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExplodeMaxDamage,
		ProjectileSetting.ExplodeMaxDamage * 0.2f,
		GetActorLocation(),
		ProjectileSetting.ProjectileMinRadiusDamage,
	    ProjectileSetting.ProjectileMaxRadiusDamage,
		5,
		/*NULL, IgnoredActor, this,nullptr*/
		UDamageType::StaticClass(), IgnoredActor,this,nullptr,ECC_Visibility);

	this->Destroy();
}

void AProjectileDefault_Grenade::SpawnGrenadeSound_Multicast_Implementation(USoundBase* GrenadeSound)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), GrenadeSound, GetActorLocation());
}

void AProjectileDefault_Grenade::SpawnGrenadeFx_Multicast_Implementation(UParticleSystem* GrenadeFx)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), GrenadeFx, GetActorLocation(), GetActorRotation(), FVector(1.0f));
}
