// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "StateEffects/TDS_cppStateEffect.h"
#include "FuncLibrary/Types.h"
#include "TDS_cppIGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTDS_cppIGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TDS_CPP_API ITDS_cppIGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	//UFUNCTION(BlueprintCallable,BlueprintImplementableEvent,Category = "Event")
	//	void AviableForEffectsBP();
	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Event")
	//	bool AviableForEffects();

	virtual EPhysicalSurface GetSurfaceType(); 
	virtual TArray<UTDS_cppStateEffect*> GetAllCurrentEffects();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void RemoveEffect(UTDS_cppStateEffect* RemoveEffect);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void AddEffect(UTDS_cppStateEffect* newEffect);

	//inventory
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropWeaponToWorld(FDropItem DropItemInfo);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropAmmoToWorld(EWeaponType TypeAmmo, int32 Cout);

};
