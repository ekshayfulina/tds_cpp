// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS_cppExplodingStructure.generated.h"

UCLASS()
class TDS_CPP_API ATDS_cppExplodingStructure : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDS_cppExplodingStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Explosion RADIUS
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosion", Replicated)
	float StructureExplosionRadius;

	// Explosion DAMAGE
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosion", Replicated)
	float StructureExplosionDamage;

	//FX
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "ExplosionSound")
	USoundBase* StructureExplosionSound = nullptr;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "ExplosionEffect")
	UParticleSystem* StructureExplosionEffect = nullptr;

	// Server RPC to trigger the explosion
	UFUNCTION(Server, Reliable)
	void StructureExplode_OnServer(AActor* DamageCauser);

	// Multicast RPC to play explosion effects
	UFUNCTION(NetMulticast, Reliable)
	void StructureExplode_Multicast();

	// Function to handle explosion logic
	void Explode(AActor* DamageCauser);


};
