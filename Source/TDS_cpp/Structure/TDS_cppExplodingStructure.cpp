// Fill out your copyright notice in the Description page of Project Settings.


#include "Structure/TDS_cppExplodingStructure.h"
#include "TDS_cppEnemyCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ATDS_cppExplodingStructure::ATDS_cppExplodingStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;
}

// Called when the game starts or when spawned
void ATDS_cppExplodingStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDS_cppExplodingStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATDS_cppExplodingStructure::StructureExplode_OnServer_Implementation(AActor* DamageCauser)
{
	StructureExplode_Multicast();
	Explode(DamageCauser);
}

void ATDS_cppExplodingStructure::StructureExplode_Multicast_Implementation()
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), StructureExplosionEffect, GetActorLocation());
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), StructureExplosionSound, GetActorLocation());
}

void ATDS_cppExplodingStructure::Explode(AActor* DamageCauser)
{
	AController* InstigatorController = GetInstigatorController();
	// Draw Debbug Explosion Sphere
	DrawDebugSphere(GetWorld(), GetActorLocation(), StructureExplosionRadius, 12, FColor::White, false, 2.0f);

	TArray<AActor*> OverlappingActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATDS_cppEnemyCharacter::StaticClass(), OverlappingActors);

	for (AActor* Actor : OverlappingActors)
	{
		float Distance = FVector::Dist(GetActorLocation(), Actor->GetActorLocation());
		if (Distance <= StructureExplosionRadius)
		{
			UGameplayStatics::ApplyDamage(Actor, StructureExplosionDamage, InstigatorController, DamageCauser, nullptr);
		}
	}

	// Destroy the exploding structure
	Destroy();
}




void ATDS_cppExplodingStructure::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate properties to all clients
	DOREPLIFETIME(ATDS_cppExplodingStructure, StructureExplosionRadius);
	DOREPLIFETIME(ATDS_cppExplodingStructure, StructureExplosionDamage);
	DOREPLIFETIME(ATDS_cppExplodingStructure, StructureExplosionSound);
	DOREPLIFETIME(ATDS_cppExplodingStructure, StructureExplosionEffect);
}
