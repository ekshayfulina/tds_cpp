// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_cppEnvironmentStructure.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine/ActorChannel.h"
#include "Kismet/GameplayStatics.h"
#include "Components/AudioComponent.h"

// Sets default values
ATDS_cppEnvironmentStructure::ATDS_cppEnvironmentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetReplicates(true);
}

// Called when the game starts or when spawned
void ATDS_cppEnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDS_cppEnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ATDS_cppEnvironmentStructure::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myStaticMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myStaticMesh)
	{
		UMaterialInterface* myMaterial = myStaticMesh->GetMaterial(0);
		if (myMaterial)
		{
			Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return Result;
}

TArray<UTDS_cppStateEffect*> ATDS_cppEnvironmentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void ATDS_cppEnvironmentStructure::RemoveEffect_Implementation(UTDS_cppStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
	if (!RemoveEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(RemoveEffect, false);
		EffectRemove = RemoveEffect;
	}
}

void ATDS_cppEnvironmentStructure::AddEffect_Implementation(UTDS_cppStateEffect* newEffect)
{
	Effects.Add(newEffect);
	if (!newEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(newEffect, true);
		EffectAdd = newEffect;
	}
	else
	{
		int32 RndIndex = FMath::RandHelper(newEffect->RandomParticleEffect.Num());
		if (newEffect->RandomParticleEffect[RndIndex])
		{
			ExecuteEffectAdded_OnServer(newEffect->RandomParticleEffect[RndIndex]);
		}
	}
}

void ATDS_cppEnvironmentStructure::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}

void ATDS_cppEnvironmentStructure::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}

void ATDS_cppEnvironmentStructure::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ATDS_cppEnvironmentStructure::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
	UTypes::ExecuteEffectAdded(ExecuteFX, this, OffsetEffect, NAME_None);
}

void ATDS_cppEnvironmentStructure::SwitchEffect(UTDS_cppStateEffect* Effect, bool bIsAdd)
{
	if (bIsAdd)
	{
		int32 RndIndex = FMath::RandHelper(Effect->RandomParticleEffect.Num());
		if (Effect)
		{
			if (Effect->RandomParticleEffect.IsValidIndex(RndIndex) && Effect->RandomParticleEffect[RndIndex])
			{
				FName NameBoneToAttached = Effect->NameBone;
				FVector Loc = FVector(0);
				USceneComponent* mySceneComp = GetRootComponent();
				if (mySceneComp)
				{
					UParticleSystemComponent* newParticleSystem = UGameplayStatics::SpawnEmitterAttached(Effect->RandomParticleEffect[RndIndex], mySceneComp, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
					ParticleSystemEffects.Add(newParticleSystem);

					if (Effect->EffectSound)
					{
						UAudioComponent* newAudioComponent = UGameplayStatics::SpawnSoundAttached(Effect->EffectSound, mySceneComp, NameBoneToAttached, FVector::ZeroVector, EAttachLocation::KeepRelativeOffset, true);
						Audioffects.Add(newAudioComponent);
					}

				}
			}
		}
	}
	else
	{
		int32 i = 0;
		bool bIsFind = false;
		if (ParticleSystemEffects.Num() > 0)
		{
			while (i < ParticleSystemEffects.Num() && !bIsFind)
			{
				int32 RndIndex = FMath::RandHelper(Effect->RandomParticleEffect.Num());
				if (ParticleSystemEffects[i]->Template && Effect->RandomParticleEffect[RndIndex] && Effect->RandomParticleEffect[RndIndex] == ParticleSystemEffects[i]->Template)
				{
					bIsFind = true;
					ParticleSystemEffects[i]->DeactivateSystem();
					ParticleSystemEffects[i]->DestroyComponent();
					ParticleSystemEffects.RemoveAt(i);

					//destroy audio effects
					if (Audioffects.Num() > 0 && Audioffects.IsValidIndex(i))
					{
						Audioffects[i]->Stop();
						Audioffects[i]->DestroyComponent();
						Audioffects.RemoveAt(i);
					}
				}
				i++;
			}
		}

	}
}

bool ATDS_cppEnvironmentStructure::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i]) { Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags); }
	}

	return Wrote;
}

void ATDS_cppEnvironmentStructure::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	DOREPLIFETIME(ATDS_cppEnvironmentStructure, Effects);
	DOREPLIFETIME(ATDS_cppEnvironmentStructure, EffectAdd);
	DOREPLIFETIME(ATDS_cppEnvironmentStructure, EffectRemove);
}

