// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interface/TDS_cppIGameActor.h"
#include "StateEffects/TDS_cppStateEffect.h"
#include "TDS_cppEnvironmentStructure.generated.h"

UCLASS()
class TDS_CPP_API ATDS_cppEnvironmentStructure : public AActor, public ITDS_cppIGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDS_cppEnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	EPhysicalSurface GetSurfaceType() override;

	TArray<UTDS_cppStateEffect*> GetAllCurrentEffects() override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void RemoveEffect(UTDS_cppStateEffect* RemoveEffect);
	void RemoveEffect_Implementation(UTDS_cppStateEffect* RemoveEffect)override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void AddEffect(UTDS_cppStateEffect* newEfect);
	void AddEffect_Implementation(UTDS_cppStateEffect* newEfect)override;

	//Effect
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<UTDS_cppStateEffect*> Effects;

	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
	UTDS_cppStateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
	UTDS_cppStateEffect* EffectRemove = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TArray<UParticleSystemComponent*> ParticleSystemEffects;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TArray<UAudioComponent*> Audioffects;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	FVector OffsetEffect = FVector(0);

	UFUNCTION()
	void EffectAdd_OnRep();
	UFUNCTION()
	void EffectRemove_OnRep();

	UFUNCTION(Server, Reliable)
	void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteFX);
	UFUNCTION(NetMulticast, Reliable)
	void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteFX);

	UFUNCTION()
	void SwitchEffect(UTDS_cppStateEffect* Effect, bool bIsAdd);

};
