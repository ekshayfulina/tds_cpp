// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Interface/TDS_cppIGameActor.h"
#include "TDS_cppEnemyCharacter.generated.h"

UCLASS()
class TDS_CPP_API ATDS_cppEnemyCharacter : public ACharacter, public ITDS_cppIGameActor
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATDS_cppEnemyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Net Effects
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void RemoveEffect(UTDS_cppStateEffect* RemoveEffect);
	void RemoveEffect_Implementation(UTDS_cppStateEffect* RemoveEffect)override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void AddEffect(UTDS_cppStateEffect* newEffect);
	void AddEffect_Implementation(UTDS_cppStateEffect* newEffect)override;

	//Effect
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<UTDS_cppStateEffect*> Effects;

	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
	UTDS_cppStateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
	UTDS_cppStateEffect* EffectRemove = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TArray<UParticleSystemComponent*> ParticleSystemEffects;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TArray<UAudioComponent*> Audioffects;

	//OnRep func
	UFUNCTION()
	void EffectAdd_OnRep();
	UFUNCTION()
	void EffectRemove_OnRep();

	UFUNCTION(Server, Reliable)
	void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteFX);
	UFUNCTION(NetMulticast, Reliable)
	void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteFX);

	UFUNCTION()
	void SwitchEffect(UTDS_cppStateEffect* Effect, bool bIsAdd);

};
