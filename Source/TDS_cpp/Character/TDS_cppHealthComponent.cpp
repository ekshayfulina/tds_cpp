// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/TDS_cppHealthComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UTDS_cppHealthComponent::UTDS_cppHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicatedByDefault(true);

	// ...
}


// Called when the game starts
void UTDS_cppHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UTDS_cppHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UTDS_cppHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UTDS_cppHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

bool UTDS_cppHealthComponent::GetIsAlive()
{
	return bIsAlive;
}

void UTDS_cppHealthComponent::ChangeHealthValue_OnServer_Implementation(float ChangeValue)
{
	if (bIsAlive)
	{
		if (!bIsInvulnerable)
		{
			ChangeValue = ChangeValue * CoefDamage;
			Health += ChangeValue;

			HealthChangeEvent_Multicast(Health, ChangeValue);
			//OnHealthChange.Broadcast(Health, ChangeValue);

			if (Health > 100.0f)
			{
				Health = 100.0f;
			}
			else
			{
				if (Health < 0.0f)
				{
					bIsAlive = false;
					DeadEvent_Multicast();
					//OnDead.Broadcast();
				}
			}
		}
	}
}

void UTDS_cppHealthComponent::SetInvulnerability(bool bInvulnerable)
{
	bIsInvulnerable = bInvulnerable;
}

void UTDS_cppHealthComponent::HealthChangeEvent_Multicast_Implementation(float NewHealth, float Vulue)
{
	OnHealthChange.Broadcast(NewHealth, Vulue);
}

void UTDS_cppHealthComponent::DeadEvent_Multicast_Implementation()
{
	OnDead.Broadcast();
}

void UTDS_cppHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTDS_cppHealthComponent, Health);
	DOREPLIFETIME(UTDS_cppHealthComponent, bIsAlive);
}
