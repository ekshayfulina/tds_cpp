// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/TDS_cppAnimNotifyRagdoll.h"
#include "TDS_cppCharacter.h"

void UTDS_cppAnimNotifyRagdoll::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	if (MeshComp && MeshComp->GetOwner())
	{
		ATDS_cppCharacter* myChar = Cast<ATDS_cppCharacter>(MeshComp->GetOwner());
		if (myChar)
		{
			myChar->EnableRagdoll_Multicast();
		}
	}
}
