// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/TDS_cppCharacterHealthComponent.h"
#include "TDS_cppCharacter.h"
#include "Kismet/GameplayStatics.h"
//#include "Net/UnrealNetwork.h"

void UTDS_cppCharacterHealthComponent::ChangeHealthValue_OnServer(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;
	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
		if (Shield <= 0.0f)
		{
			//FX shield damage
			int32 RndIndex = FMath::RandHelper(ShieldDamageEffects.Num());
			if (ShieldDamageEffects.IsValidIndex(RndIndex) && ShieldDamageEffects[RndIndex])
			{
				//FX array spawn
				if (ATDS_cppCharacter* myCharacter = Cast<ATDS_cppCharacter>(GetOwner()))
				{
					USkeletalMeshComponent* myCharacterMesh = myCharacter->GetMesh();
					UGameplayStatics::SpawnEmitterAttached(ShieldDamageEffects[RndIndex], myCharacterMesh, FName("DamageShieldEffect"), FVector(0.0f,0.0f,130.0f),
						FRotator::ZeroRotator, FVector(1.0f), EAttachLocation::KeepRelativeOffset, true);

				//spawn shield damage sound 
					UGameplayStatics::SpawnSoundAttached(ShieldDamageSound, myCharacterMesh, FName("DamageShieldSound"), FVector::ZeroVector, EAttachLocation::KeepRelativeOffset, true);
				}
			}
			/*UE_LOG(LogTemp, Warning, TEXT("UTDS_cppCharacterHealthComponent::ChangeHealthValue - Sheild < 0"));*/
		}
	}
	else
	{
		Super::ChangeHealthValue_OnServer(ChangeValue);
		//FX cascade bloodfpr caracter ?
	}
	//TO DO
	UpdateShieldMaterial();
	//OnShieldMaterialChange.AddDynamic(this, &UTDS_cppCharacterHealthComponent::ChangeShieldMaterial_BP);
}

float UTDS_cppCharacterHealthComponent::GetCurrentShield()											
{
	return Shield;
}

void UTDS_cppCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;

	ShieldChangeEvent_Multicast(Shield, ChangeValue);
	//OnShieldChange.Broadcast(Shield, ChangeValue);

	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else 
	{
		if (Shield < 0.0f)
			Shield = 0.0f;
	}
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CoolDownShielldTimer, this, &UTDS_cppCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieRecoveryRateTimer);
	}
	//TO DO
	UpdateShieldMaterial();
	//OnShieldMaterialChange.AddDynamic(this, &UTDS_cppCharacterHealthComponent::ChangeShieldMaterial_BP);
}

void UTDS_cppCharacterHealthComponent::CoolDownShieldEnd()
{
	if (ATDS_cppCharacter* myCharacter = Cast<ATDS_cppCharacter>(GetOwner()))
	{
		if (bIsAlive)
		{
			if (GetWorld())
			{
				GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieRecoveryRateTimer, this, &UTDS_cppCharacterHealthComponent::RecoveryShield, ShieldRecoverRate, true);
			}
		}
	}
}

void UTDS_cppCharacterHealthComponent::RecoveryShield()
{
	if (ATDS_cppCharacter* myCharacter = Cast<ATDS_cppCharacter>(GetOwner()))
	{
		if (bIsAlive)
		{
			float tmp = Shield;
			tmp = tmp + ShieldRecoverValue;
			if (tmp > 100.0f)
			{
				Shield = 100.0f;
				if (GetWorld())
				{
					GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieRecoveryRateTimer);

					//FX and sound of RecoveryShield logic
					int32 RndIndex = FMath::RandHelper(ShieldRecoveryEffects.Num());
					if (ShieldRecoveryEffects.IsValidIndex(RndIndex) && ShieldRecoveryEffects[RndIndex])
					{
						//FX array spawn
						if (/*ATDS_cppCharacter* myCharacter = Cast<ATDS_cppCharacter>(GetOwner())*/ myCharacter)
						{
							USkeletalMeshComponent* myCharacterMesh = myCharacter->GetMesh();
							UGameplayStatics::SpawnEmitterAttached(ShieldRecoveryEffects[RndIndex], myCharacterMesh, FName("RecoveryShieldEffect"), FVector(0.0f, 0.0f, 0.0f),
								FRotator::ZeroRotator, FVector(1.3f), EAttachLocation::KeepRelativeOffset, true);
							//spawn shield recovery sound
							UGameplayStatics::SpawnSoundAttached(ShieldRecoverySound, myCharacterMesh, FName("RecoveryShieldSound"), FVector::ZeroVector, EAttachLocation::KeepRelativeOffset, true);
						}
					}
				}
			}
			else
			{
				Shield = tmp;
			}
			ShieldChangeEvent_Multicast(Shield, ShieldRecoverValue);
			UpdateShieldMaterial();
			//OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
		}
	}
}

void UTDS_cppCharacterHealthComponent::UpdateShieldMaterial()
{
	//set shield material
	if (Shield > 0.0f && Shield < 100.0f)
	{
		if (ATDS_cppCharacter* myCharacter = Cast<ATDS_cppCharacter>(GetOwner()))
		{
			USkeletalMeshComponent* myCharacterSkeletalMesh = myCharacter->GetMesh();
			myCharacterSkeletalMesh->SetMaterial(0, ShieldMaterial);
		}
	}
	//return to normal material
	else
	{
		if (ATDS_cppCharacter* myCharacter = Cast<ATDS_cppCharacter>(GetOwner()))
		{
			USkeletalMeshComponent* myCharacterSkeletalMesh = myCharacter->GetMesh();
			//to do bp event to set material to origin
			//myCharacterSkeletalMesh->SetMaterial(0, OriginalMaterial);
			ChangeBodyOriginalMaterial_Multicast();
			//OnShieldMaterialChange.Broadcast();
		}
	}
}

void UTDS_cppCharacterHealthComponent::ChangeBodyOriginalMaterial_Multicast_Implementation()
{
	OnShieldMaterialChange.Broadcast();
}


float UTDS_cppCharacterHealthComponent::GetShieldValue()
{
	return Shield;
}

void UTDS_cppCharacterHealthComponent::ShieldChangeEvent_Multicast_Implementation(float NewShied, float Damage)
{
	OnShieldChange.Broadcast(NewShied, Damage);
}

//void UTDS_cppCharacterHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
//{
//	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
//
//	DOREPLIFETIME(UTDS_cppCharacterHealthComponent, OriginalMaterial);
//	DOREPLIFETIME(UTDS_cppCharacterHealthComponent, ShieldMaterial);
//}