// Copyright Epic Games, Inc. All Rights Reserved.

#include "Character/TDS_cppCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "Game/TDS_cppGameInstance.h"
#include "Weapons/Projectiles/ProjectileDefault.h"
#include "TDS_cpp.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/AudioComponent.h"
#include "Engine/ActorChannel.h"

ATDS_cppCharacter::ATDS_cppCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	//HemetMesh
	/*StaticMeshHelmet = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Helmet"));
	StaticMeshHelmet->SetupAttachment();*/

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	//CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	//CursorToWorld->SetupAttachment(RootComponent);
	//static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	//if (DecalMaterialAsset.Succeeded())
	//{
	//	CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	//}
	//CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	//CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	//create InventoryConmponent and CharHealthComponent
	InventoryComponent = CreateDefaultSubobject<UTDS_cppInventoryComponent>(TEXT("InventoryComponent"));
	CharHealthComponent = CreateDefaultSubobject<UTDS_cppCharacterHealthComponent>(TEXT("HealthComponent"));

	if (CharHealthComponent)
	{
		CharHealthComponent->OnDead.AddDynamic(this, &ATDS_cppCharacter::CharDead);
	}

	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATDS_cppCharacter::InitWeapon);
	}

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	//NetWork 
	bReplicates = true;

}

void ATDS_cppCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	/*if (CursorToWorld != nullptr)
	{
		if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}*/

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC && myPC->IsLocalPlayerController())
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}
	MovementTick(DeltaSeconds);

	FString currentstring = "None";
	if (CurrentCursor)
	{
		currentstring = CurrentCursor->GetName();
	}

	//UE_LOG(LogTPS_Net, Log, TEXT(" current cursor = %s"), *currentstring);

}

void ATDS_cppCharacter::BeginPlay()
{
	Super::BeginPlay();
	//bool bIsSpawnCursor = true;
	if (GetWorld() && GetWorld()->GetNetMode() != NM_DedicatedServer)
	{
		//bIsSpawnCursor = false;
	}
	if (CursorMaterial && GetLocalRole() == ROLE_AutonomousProxy || GetLocalRole() == ROLE_Authority)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}

	/*InitWeapon(InitWeaponName);*/
}

void ATDS_cppCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);
	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDS_cppCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDS_cppCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("ChangeToSprint"), EInputEvent::IE_Pressed, this, &ATDS_cppCharacter::InputSprintPressed);
	NewInputComponent->BindAction(TEXT("ChangeToWalk"), EInputEvent::IE_Pressed, this, &ATDS_cppCharacter::InputWalkPressed);
	NewInputComponent->BindAction(TEXT("AimEvent"), EInputEvent::IE_Pressed, this, &ATDS_cppCharacter::InputAimPressed);
	NewInputComponent->BindAction(TEXT("ChangeToSprint"), EInputEvent::IE_Released, this, &ATDS_cppCharacter::InputSprintReleased);
	NewInputComponent->BindAction(TEXT("ChangeToWalk"), EInputEvent::IE_Released, this, &ATDS_cppCharacter::InputWalkReleased);
	NewInputComponent->BindAction(TEXT("AimEvent"), EInputEvent::IE_Released, this, &ATDS_cppCharacter::InputAimReleased);


	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATDS_cppCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATDS_cppCharacter::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATDS_cppCharacter::TryReloadWeapon);

	NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ATDS_cppCharacter::TrySwitchNextWeapon);
	NewInputComponent->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &ATDS_cppCharacter::TrySwitchPreviosWeapon);

	NewInputComponent->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Pressed, this, &ATDS_cppCharacter::TryAbilityEnabled_OnServer);

	NewInputComponent->BindAction(TEXT("DropCurrentWeapon"), EInputEvent::IE_Pressed, this, &ATDS_cppCharacter::DropCurrentWeapon);

	TArray<FKey> HotKeys;
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);
	HotKeys.Add(EKeys::Zero);

	NewInputComponent->BindKey(HotKeys[1], IE_Pressed, this, &ATDS_cppCharacter::TKeyPressed<1>);
	NewInputComponent->BindKey(HotKeys[2], IE_Pressed, this, &ATDS_cppCharacter::TKeyPressed<2>);
	NewInputComponent->BindKey(HotKeys[3], IE_Pressed, this, &ATDS_cppCharacter::TKeyPressed<3>);
	NewInputComponent->BindKey(HotKeys[4], IE_Pressed, this, &ATDS_cppCharacter::TKeyPressed<4>);
	NewInputComponent->BindKey(HotKeys[5], IE_Pressed, this, &ATDS_cppCharacter::TKeyPressed<5>);
	NewInputComponent->BindKey(HotKeys[6], IE_Pressed, this, &ATDS_cppCharacter::TKeyPressed<6>);
	NewInputComponent->BindKey(HotKeys[7], IE_Pressed, this, &ATDS_cppCharacter::TKeyPressed<7>);
	NewInputComponent->BindKey(HotKeys[8], IE_Pressed, this, &ATDS_cppCharacter::TKeyPressed<8>);
	NewInputComponent->BindKey(HotKeys[9], IE_Pressed, this, &ATDS_cppCharacter::TKeyPressed<9>);
	NewInputComponent->BindKey(HotKeys[0], IE_Pressed, this, &ATDS_cppCharacter::TKeyPressed<0>);

}


void ATDS_cppCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATDS_cppCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATDS_cppCharacter::InputAttackPressed()
{
	if (CharHealthComponent && CharHealthComponent->GetIsAlive())
	{
		AttackCharEvent(true);
	}
}

void ATDS_cppCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATDS_cppCharacter::InputWalkPressed()
{
	WalkEnabled = true;
	ChangeMovementState();
}

void ATDS_cppCharacter::InputWalkReleased()
{
	WalkEnabled = false;
	ChangeMovementState();
}

void ATDS_cppCharacter::InputSprintPressed()
{
	if (StaminaScale > 0.0f && !AimEnabled)
	{
		SprintRunEnabled = true;
		ChangeMovementState();
		/*MovementState = EMovementState::SprintRun_State;
		CharacterUpdate();*/
		GetWorldTimerManager().SetTimer(StaminaDrainTimerHandle, this, &ATDS_cppCharacter::DrainStamina, 0.1f, true);
	}
}

void ATDS_cppCharacter::InputSprintReleased()
{
	SprintRunEnabled = false;
	ChangeMovementState();
	GetWorldTimerManager().ClearTimer(StaminaDrainTimerHandle);

	if (StaminaScale < 100.0f)
	{
		GetWorldTimerManager().SetTimer(StaminaRecoveryTimerHandle, this, &ATDS_cppCharacter::RecoveryStamina, 0.1f, true);
	}
}

void ATDS_cppCharacter::InputAimPressed()
{
	AimEnabled = true;
	ChangeMovementState();
}

void ATDS_cppCharacter::InputAimReleased()
{
	AimEnabled = false;
	ChangeMovementState();
}

void ATDS_cppCharacter::MovementTick(float Deltatime)
{
	if (CharHealthComponent && CharHealthComponent->GetIsAlive())
	{
		if (GetController() && GetController()->IsLocalPlayerController())
		{
			AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
			AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

			/*FString SEnum = UEnum::GetValueAsString(GetMovementState());
			UE_LOG(LogTDS_cpp_Net, Warning, TEXT("Movement state - %s"), *SEnum);*/
			if (MovementState == EMovementState::SprintRun_State)
			{
				FVector myRotationVector = FVector(AxisX, AxisY, 0.0f);
				FRotator myRotator = myRotationVector.ToOrientationRotator();
				/*((FQuat(myRotator)));

				SetActorRotationByYaw_OnServer(myRotator.Yaw);*/
				SetActorRotation((FQuat(myRotator)));
				SetActorRotationByYaw_OnServer(myRotator.Yaw);

			}
			else
			{
				APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

				if (MyController)
				{
					FHitResult ResultHit;
					//MyController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);  bug was here Config/DefaultEngine.ini
					MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

					float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
					SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));

					SetActorRotationByYaw_OnServer(FindRotatorResultYaw);

					if (CurrentWeapon)
					{
						FVector Displacement = FVector(0);
						bool bIsReduceDispersion = false;
						switch (MovementState)
						{
						case EMovementState::Aim_State:
							Displacement = FVector(0.0f, 0.0f, 160.0f);
							//CurrentWeapon->ShouldReduceDispersion = true;
							bIsReduceDispersion = true;
							break;
						case EMovementState::AimWalk_State:
							//CurrentWeapon->ShouldReduceDispersion = true;
							Displacement = FVector(0.0f, 0.0f, 160.0f);
							bIsReduceDispersion = true;
							break;
						case EMovementState::Walk_State:
							Displacement = FVector(0.0f, 0.0f, 120.0f);
							//CurrentWeapon->ShouldReduceDispersion = false;
							break;
						case EMovementState::Run_State:
							Displacement = FVector(0.0f, 0.0f, 120.0f);
							//CurrentWeapon->ShouldReduceDispersion = false;
							break;
						case EMovementState::SprintRun_State:
							break;
						default:
							break;
						}


						//CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
						//aim cursor like 3d Widget?
						CurrentWeapon->UpdateWeaponByCharacterMovementState_OnServer(ResultHit.Location + Displacement, bIsReduceDispersion);
					}
				}
			}
		}

	}
	
}

void ATDS_cppCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->SetWeaponStateFire_OnServer(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATDS_cppCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}


void ATDS_cppCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeedNormal;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementInfo.AimSpeedWalk;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeedNormal;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeedNormal;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = MovementInfo.SprintRunSpeedRun;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDS_cppCharacter::DrainStamina()
{
	if (StaminaDrainRate > 0.0f && SprintRunEnabled && MovementState == EMovementState::SprintRun_State)
	{
		StaminaScale -= StaminaDrainRate;

		if (StaminaScale <= 0.0f)
		{
			StaminaScale = 0.0f;
			// Stop draining STAMINA
			GetWorldTimerManager().ClearTimer(StaminaDrainTimerHandle);

			//Movement->Run_State
			MovementState = EMovementState::Run_State;
			CharacterUpdate();
			SprintRunEnabled = false;
		}
	}
	// Notify STAMINA changes
	NotifyStaminaChanged();
}

void ATDS_cppCharacter::RecoveryStamina()
{
	if (StaminaRecoveryRate >= 0.0f && !SprintRunEnabled && MovementState != EMovementState::SprintRun_State)
	{
		StaminaScale += StaminaRecoveryRate;

		if (StaminaScale >= 100.0f)
		{
			StaminaScale = 100.0f;
			// Stop recovering STAMINA
			GetWorldTimerManager().ClearTimer(StaminaRecoveryTimerHandle);
		}
	}
	// Notify STAMINA changes
	NotifyStaminaChanged();
}

void ATDS_cppCharacter::NotifyStaminaChanged()
{
	StaminaChangedEvent_Multicast(StaminaScale);
	//OnStaminaChanged.Broadcast(StaminaScale);
}

void ATDS_cppCharacter::ChangeMovementState() 
{	
	EMovementState NewState = EMovementState::Run_State;
	//changing movement states
	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	{
		NewState = EMovementState::Run_State;
	}
	else
	{
		if (SprintRunEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;
			NewState = EMovementState::SprintRun_State;
		}
		else
		{
			if (WalkEnabled && !SprintRunEnabled && AimEnabled)
			{
				NewState = EMovementState::AimWalk_State;
			}
			else
			{
				if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
				{
					NewState = EMovementState::Walk_State;
				}
				else
				{
					if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
					{
						NewState = EMovementState::Aim_State;
					}
				}
			}
		}
	}

	SetMovementState_OnServer(NewState);
	//CharacterUpdate();
	
	// Notify STAMINA changes
	NotifyStaminaChanged();

	//Weapon state update										  
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon_OnServer(NewState);
	}
} 

AWeaponDefault* ATDS_cppCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATDS_cppCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	///OnServer
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UTDS_cppGameInstance* myGameInstance = Cast<UTDS_cppGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGameInstance)
	{
		if (myGameInstance->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSetting = myWeaponInfo;
					//myWeapon->AdditionalWeaponInfo.Round = myWeaponInfo.MaxRound;
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon_OnServer(MovementState);

					myWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;
					//if(InventoryComponent)
					CurrentIndexWeapon = NewCurrentIndexWeapon;//fix

					//Not Forget remove delegate on change/drop weapon
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDS_cppCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDS_cppCharacter::WeaponReloadEnd);
					myWeapon->OnWeaponFireStart.AddDynamic(this, &ATDS_cppCharacter::WeaponFireStart);

					// after switch try reload weapon if needed
					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
						CurrentWeapon->InitReload();

					if (InventoryComponent)
						InventoryComponent->OnWeaponAmmoAvailable.Broadcast(myWeapon->WeaponSetting.WeaponType);
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATDS_cppCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}

void ATDS_cppCharacter::RemoveCurrentWeapon()
{
}

void ATDS_cppCharacter::TryReloadWeapon()
{
	if (CharHealthComponent && CharHealthComponent->GetIsAlive() && CurrentWeapon && !CurrentWeapon->WeaponReloading)
	{
		TryReloadWeapon_OnServer();
	}
}
void ATDS_cppCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void ATDS_cppCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}
	WeaponReloadEnd_BP(bIsSuccess);
}

void ATDS_cppCharacter::TrySwitchWeaponToIndexByKeyInput_OnServer_Implementation(int32 ToIndex)
{
	bool bIsSuccess = false;
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.IsValidIndex(ToIndex))
	{
		if (CurrentIndexWeapon != ToIndex && InventoryComponent)
		{
			int32 OldIndex = CurrentIndexWeapon;
			FAdditionalWeaponInfo OldInfo;

			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->AdditionalWeaponInfo;
				if (CurrentWeapon->WeaponReloading)
					CurrentWeapon->CancelReload();
			}

			bIsSuccess = InventoryComponent->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);
		}
	}
}

void ATDS_cppCharacter::DropCurrentWeapon()
{
	if (InventoryComponent)
	{
		FDropItem ItemInfo;
		InventoryComponent->DropWeaponByIndex_OnServer(CurrentIndexWeapon);
	}
}

void ATDS_cppCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	//in BP
}

void ATDS_cppCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	//in BP
}

void ATDS_cppCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);

	WeaponFireStart_BP(Anim);
}

void ATDS_cppCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

UDecalComponent* ATDS_cppCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

EMovementState ATDS_cppCharacter::GetMovementState()
{
	//return EMovementState();
	return MovementState;
}

TArray<UTDS_cppStateEffect*> ATDS_cppCharacter::GetCurrentEffectsOnChar()
{
	return Effects;															   
}

int32 ATDS_cppCharacter::GetCurrentWeaponIndex()
{
	return CurrentIndexWeapon;
}

bool ATDS_cppCharacter::GetIsAlive()
{
	//return bIsAlive;
	bool result = false;
	if (CharHealthComponent)
	{
		result = CharHealthComponent->GetIsAlive();
	}
	return result;
}

//ToDO in one func TrySwitchPreviosWeapon && TrySwicthNextWeapon
void ATDS_cppCharacter::TrySwitchNextWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{
			}
		}
	}

}
//in one func
void ATDS_cppCharacter::TrySwitchPreviosWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			//InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->AdditionalWeaponInfo);
			if (InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{
			}
		}
	}
}

void ATDS_cppCharacter::TryAbilityEnabled_OnServer_Implementation()
{
	TryAbilityEnabled_Multicast();
}

void ATDS_cppCharacter::TryAbilityEnabled_Multicast_Implementation()
{
	if (AbilityEffect)//TODO Cool down
	{
		UTDS_cppStateEffect* NewEffect = NewObject<UTDS_cppStateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this, NAME_None);
		}
	}
}

EPhysicalSurface ATDS_cppCharacter::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	if (CharHealthComponent)
	{
		if (CharHealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
				if (myMaterial)
				{
					Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}		
	}			
	return Result;

}

TArray<UTDS_cppStateEffect*> ATDS_cppCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void ATDS_cppCharacter::RemoveEffect_Implementation(UTDS_cppStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
	if (!RemoveEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(RemoveEffect, false);
		EffectRemove = RemoveEffect;
	}
}

void ATDS_cppCharacter::AddEffect_Implementation(UTDS_cppStateEffect* newEffect)
{
	Effects.Add(newEffect);

	if (!newEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(newEffect, true);
		EffectAdd = newEffect;
	}
	else
	{
		int32 RndIndex = FMath::RandHelper(newEffect->RandomParticleEffect.Num());
		if (newEffect->RandomParticleEffect[RndIndex])
		{
			ExecuteEffectAdded_OnServer(newEffect->RandomParticleEffect[RndIndex]);
		}
	}
}

void ATDS_cppCharacter::CharDead_BP_Implementation()
{
	//BP
}

void ATDS_cppCharacter::SetActorRotationByYaw_OnServer_Implementation(float Yaw)
{
	SetActorRotationByYaw_Multicast(Yaw);
}

void ATDS_cppCharacter::SetActorRotationByYaw_Multicast_Implementation(float Yaw)
{
	if (Controller && !Controller->IsLocalPlayerController())
	{
		SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
	}
}																										 

void ATDS_cppCharacter::SetMovementState_OnServer_Implementation(EMovementState NewState)
{
	SetMovementState_Multicast(NewState);
}

void ATDS_cppCharacter::SetMovementState_Multicast_Implementation(EMovementState NewState)
{
	MovementState = NewState; 
	CharacterUpdate();
}

void ATDS_cppCharacter::TryReloadWeapon_OnServer_Implementation()
{
	if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
	{
		CurrentWeapon->InitReload();
		AimEnabled = false;
	}
}

void ATDS_cppCharacter::CharDead()
{
	CharDead_BP();
	if (HasAuthority())
	{
		float TimeAnim = 0.0f;
		int32 rnd = FMath::RandHelper(DeadsAnim.Num());
		if (DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
		{
			TimeAnim = DeadsAnim[rnd]->GetPlayLength();
			PlayAnim_Multicast(DeadsAnim[rnd]);
			//GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
		}
		//bIsAlive = false;

		if (GetController())
		{
			GetController()->UnPossess();
		}

		//UnPossessed();

		float DecraeseAnimTimer = FMath::FRandRange(0.2f, 1.0f);
		//Timer RagDoll
		GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ATDS_cppCharacter::EnableRagdoll_Multicast, TimeAnim - DecraeseAnimTimer, false);

		SetLifeSpan(20.0f);
		if (GetCurrentWeapon())
		{
			GetCurrentWeapon()->SetLifeSpan(20.0f);
		}
	}
	else
	{
		if (GetCursorToWorld())
		{
			GetCursorToWorld()->SetVisibility(false);
		}
		AttackCharEvent(false);
	}
																			   
	if (GetCapsuleComponent())
	{
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
	}	
	
	//CharDead_BP();
}

void ATDS_cppCharacter::EnableRagdoll_Multicast_Implementation()
{
	if (GetMesh())
	{
		//GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
		//GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Ignore);

		//GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		//GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Block);
		GetMesh()->SetCollisionObjectType(ECC_PhysicsBody);
		GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Block);
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);

		//I was trying change weight to prevent body flying away, but it doesn't work 
		//GetMesh()->SetPhysicsBlendWeight(0.2f);
	}
}

float ATDS_cppCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (CharHealthComponent && CharHealthComponent->GetIsAlive())
	{
		CharHealthComponent->ChangeHealthValue_OnServer(-DamageAmount);
	}
	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, NAME_None, myProjectile->ProjectileSetting.Effect, GetSurfaceType());	// to do Name_None - bone for radial damage			
		}
	}

	return ActualDamage;
}

void ATDS_cppCharacter::StaminaChangedEvent_Multicast_Implementation(float Value)
{
	OnStaminaChanged.Broadcast(Value);
}

void ATDS_cppCharacter::PlayAnim_Multicast_Implementation(UAnimMontage* Anim)
{
	if (GetMesh() && GetMesh()->GetAnimInstance())
	{
		GetMesh()->GetAnimInstance()->Montage_Play(Anim);
	}
}

void ATDS_cppCharacter::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}

void ATDS_cppCharacter::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}

void ATDS_cppCharacter::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ATDS_cppCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
	UTypes::ExecuteEffectAdded(ExecuteFX, this, FVector(0), FName("Spine_01"));
}

void ATDS_cppCharacter::SwitchEffect(UTDS_cppStateEffect* Effect, bool bIsAdd)
{
	int32 RndIndex = FMath::RandHelper(Effect->RandomParticleEffect.Num());
	if (bIsAdd)
	{
		if (Effect) 
		{
			if (Effect->RandomParticleEffect.IsValidIndex(RndIndex) && Effect->RandomParticleEffect[RndIndex])
			{
				FName NameBoneToAttached = Effect->NameBone;
				FVector Loc = FVector(0);

				USkeletalMeshComponent* myMesh = GetMesh();
				if (myMesh)
				{
					UParticleSystemComponent* newPartileSystem = UGameplayStatics::SpawnEmitterAttached(Effect->RandomParticleEffect[RndIndex], myMesh, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
					ParticleSystemEffects.Add(newPartileSystem);	

					//newPartileSystem->Template
					if (Effect->EffectSound)
					{
						UAudioComponent* newAudioComponent = UGameplayStatics::SpawnSoundAttached(Effect->EffectSound, myMesh, NameBoneToAttached, FVector::ZeroVector, EAttachLocation::KeepRelativeOffset, true);
						Audioffects.Add(newAudioComponent);
					}
				}
			}
		}
		
	}
	else
	{
		if (Effect && Effect->RandomParticleEffect.IsValidIndex(RndIndex) && Effect->RandomParticleEffect[RndIndex])
		{
			int32 i = 0;
			bool bIsFind = false;
			if (ParticleSystemEffects.Num() > 0)
			{
				while (i < ParticleSystemEffects.Num() && !bIsFind)
				{
					if (ParticleSystemEffects[i]->Template && Effect->RandomParticleEffect[RndIndex] && Effect->RandomParticleEffect[RndIndex] == ParticleSystemEffects[i]->Template)
					{
						//destroy effect particles
						bIsFind = true;
						ParticleSystemEffects[i]->DeactivateSystem();
						ParticleSystemEffects[i]->DestroyComponent();
						ParticleSystemEffects.RemoveAt(i);

						//destroy audio effects
						if (Audioffects.Num() > 0 && Audioffects.IsValidIndex(i))
						{
							Audioffects[i]->Stop();
							Audioffects[i]->DestroyComponent();
							Audioffects.RemoveAt(i);
						}
					}
					i++;

				}
			}
		}
	}
}

bool ATDS_cppCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel,Bunch,RepFlags);

	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i]) 
		{ 
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags); 
		}
	}

	return Wrote;
}

void ATDS_cppCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATDS_cppCharacter, MovementState);
	DOREPLIFETIME(ATDS_cppCharacter, CurrentWeapon);
	DOREPLIFETIME(ATDS_cppCharacter, CurrentIndexWeapon);
	DOREPLIFETIME(ATDS_cppCharacter, Effects);
	DOREPLIFETIME(ATDS_cppCharacter, EffectAdd);
	DOREPLIFETIME(ATDS_cppCharacter, EffectRemove);
	DOREPLIFETIME(ATDS_cppCharacter, AbilityEffect);
}
