// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/TDS_cppEnemyCharacter.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/AudioComponent.h"
#include "Engine/ActorChannel.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
ATDS_cppEnemyCharacter::ATDS_cppEnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATDS_cppEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDS_cppEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATDS_cppEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ATDS_cppEnemyCharacter::RemoveEffect_Implementation(UTDS_cppStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
	if (!RemoveEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(RemoveEffect, false);
		EffectRemove = RemoveEffect;
	}
}

void ATDS_cppEnemyCharacter::AddEffect_Implementation(UTDS_cppStateEffect* newEffect)
{
	Effects.Add(newEffect);
	if (!newEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(newEffect, true);
		EffectAdd = newEffect;
	}
	else
	{
		int32 RndIndex = FMath::RandHelper(newEffect->RandomParticleEffect.Num());
		if (newEffect->RandomParticleEffect[RndIndex])
		{
			ExecuteEffectAdded_OnServer(newEffect->RandomParticleEffect[RndIndex]);
		}
	}
}

void ATDS_cppEnemyCharacter::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}

void ATDS_cppEnemyCharacter::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}

void ATDS_cppEnemyCharacter::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ATDS_cppEnemyCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
	UTypes::ExecuteEffectAdded(ExecuteFX, this, FVector(0), FName("Spine_01"));
}

void ATDS_cppEnemyCharacter::SwitchEffect(UTDS_cppStateEffect* Effect, bool bIsAdd)
{
	int32 RndIndex = FMath::RandHelper(Effect->RandomParticleEffect.Num());
	if (bIsAdd)
	{
		if (Effect)
		{
			if (Effect->RandomParticleEffect.IsValidIndex(RndIndex) && Effect->RandomParticleEffect[RndIndex])
			{
				FName NameBoneToAttached = Effect->NameBone;
				FVector Loc = FVector(0);

				USkeletalMeshComponent* myMesh = GetMesh();
				if (myMesh)
				{
					UParticleSystemComponent* newPartileSystem = UGameplayStatics::SpawnEmitterAttached(Effect->RandomParticleEffect[RndIndex], myMesh, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
					ParticleSystemEffects.Add(newPartileSystem);

					//newPartileSystem->Template
					if (Effect->EffectSound)
					{
						UAudioComponent* newAudioComponent = UGameplayStatics::SpawnSoundAttached(Effect->EffectSound, myMesh, NameBoneToAttached, FVector::ZeroVector, EAttachLocation::KeepRelativeOffset, true);
						Audioffects.Add(newAudioComponent);
					}
				}
			}
		}

	}
	else
	{
		int32 i = 0;
		bool bIsFind = false;
		if (ParticleSystemEffects.Num() > 0)
		{
			while (i < ParticleSystemEffects.Num() && !bIsFind)
			{
				if (ParticleSystemEffects[i]->Template && Effect->RandomParticleEffect[RndIndex] && Effect->RandomParticleEffect[RndIndex] == ParticleSystemEffects[i]->Template)
				{
					//destroy effect particles
					bIsFind = true;
					ParticleSystemEffects[i]->DeactivateSystem();
					ParticleSystemEffects[i]->DestroyComponent();
					ParticleSystemEffects.RemoveAt(i);

					//destroy audio effects
					if (Audioffects.Num() > 0 && Audioffects.IsValidIndex(i))
					{
						Audioffects[i]->Stop();
						Audioffects[i]->DestroyComponent();
						Audioffects.RemoveAt(i);
					}
				}
				i++;

			}
		}
	}
}

bool ATDS_cppEnemyCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i]) { Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags); }
	}
	return Wrote;

}

void ATDS_cppEnemyCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATDS_cppEnemyCharacter, Effects);
	DOREPLIFETIME(ATDS_cppEnemyCharacter, EffectAdd);
	DOREPLIFETIME(ATDS_cppEnemyCharacter, EffectRemove);
}
