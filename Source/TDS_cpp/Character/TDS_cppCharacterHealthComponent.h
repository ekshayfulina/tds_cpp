// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/TDS_cppHealthComponent.h"
#include "TDS_cppCharacterHealthComponent.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shied, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnShieldMaterialChange);

UCLASS()
class TDS_CPP_API UTDS_cppCharacterHealthComponent : public UTDS_cppHealthComponent
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health");
	FOnShieldChange OnShieldChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Material");
	FOnShieldMaterialChange	OnShieldMaterialChange;

	FTimerHandle TimerHandle_CoolDownShielldTimer;
	FTimerHandle TimerHandle_ShieRecoveryRateTimer;

protected:
	float Shield = 100.0f;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float CoolDownShieldRecoverTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRecoverValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRecoverRate = 0.1f;

	//FX Shield Damage
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ShieldDamageCharFX")
	TArray<UParticleSystem*> ShieldDamageEffects;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ShieldDamageCharFX")
	USoundBase* ShieldDamageSound;

	//FX ShieldRecovery
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ShieldRecoveryCharFX")
	TArray<UParticleSystem*> ShieldRecoveryEffects;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ShieldRecoveryCharFX")
	USoundBase* ShieldRecoverySound;

	//Shield Material
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "OriginalMaterial")
	UMaterialInterface* OriginalMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ShieldMaterial")
	UMaterialInterface* ShieldMaterial = nullptr;

	void ChangeHealthValue_OnServer(float ChangeValue) override;

	float GetCurrentShield();

	UFUNCTION(BlueprintCallable)
	void ChangeShieldValue(float ChangeValue);

	void CoolDownShieldEnd();
	void RecoveryShield();

	//Shield Material
	UFUNCTION()
	void UpdateShieldMaterial();
	UFUNCTION(NetMulticast, Reliable)
	void ChangeBodyOriginalMaterial_Multicast();

	UFUNCTION()
	float GetShieldValue();

	//Net
	UFUNCTION(NetMulticast, Reliable)
	void ShieldChangeEvent_Multicast(float NewShied, float Damage);
};
