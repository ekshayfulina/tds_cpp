// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "TDS_cppStateEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class TDS_CPP_API UTDS_cppStateEffect : public UObject
{
	GENERATED_BODY()

public:
	bool IsSupportedForNetworking()const override { return true;};
	virtual bool InitObject(AActor* Actor, FName NameBoneHit);
	virtual void DestroyObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	bool bIsStakable = false;

	//FX ->to do tarray of effects+randhelper
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	/*UParticleSystem* ParticleEffect = nullptr;*/
	TArray<UParticleSystem*> RandomParticleEffect;

	//UParticleSystemComponent* ParticleEmitter = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	USoundBase* EffectSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	bool bIsAutoDestroyParticleEffect = false;

	AActor* myActor = nullptr;

	UPROPERTY(Replicated)
	FName NameBone;

	UFUNCTION(NetMulticast, Reliable)
	void FXSpawnByStateEffect_Multicast(UParticleSystem* Effect, FName NameBoneHit);
	UFUNCTION(NetMulticast, Reliable)
	void SoundSpawnByStateEffect_Multicast(USoundBase* Sound);

};

UCLASS()
class TDS_CPP_API UTDS_cppStateEffect_ExecuteOnce : public UTDS_cppStateEffect
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
	float Power = 20.0f;
	//FX
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
	//USoundBase* ExecuteEffectSound;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
	//TArray<UParticleSystem*> ExecuteParticleEffect;
};

UCLASS()
class TDS_CPP_API UTDS_cppStateEffect_ExecuteTimer : public UTDS_cppStateEffect
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	virtual void Execute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	float RateTime = 1.0f;

	//Timers
	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;
};

UCLASS()
class TDS_CPP_API UTDS_cppInvulnerabilityEffect : public UTDS_cppStateEffect
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;
	void OnInvulnerabilityTimer();


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Invulnerability")
	float InvulnerabilityDuration = 10.0f;

	//Timers
	FTimerHandle TimerHandle_Invulnerability;

	//for Material changing
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Invulnerability")
	UMaterialInterface* InvulnerabilityMaterial = nullptr;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "OriginalMaterial")
	//UMaterialInterface* OriginalMaterial = nullptr;
	USkeletalMeshComponent* SkeletalMeshCharacter;
	/*int32 MaterialSlotIndex;*/ // Index of the material slot to change

	//Sound
	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Invulnerability")
	USoundBase* InvulnerabilitySound;*/
};
