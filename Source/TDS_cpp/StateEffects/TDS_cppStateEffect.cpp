// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_cppStateEffect.h"
#include "Character/TDS_cppHealthComponent.h"
#include "Interface/TDS_cppIGameActor.h"
#include "TDS_cppCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

//ParentLogic
bool UTDS_cppStateEffect::InitObject(AActor* Actor, FName NameBoneHit)
{
	myActor = Actor;

	NameBone = NameBoneHit;
	ITDS_cppIGameActor* myInterface = Cast<ITDS_cppIGameActor>(myActor);
	if (myInterface)
	{
		myInterface->Execute_AddEffect(myActor, this);
	}

	return true;

}

void UTDS_cppStateEffect::DestroyObject()
{
	ITDS_cppIGameActor* myInterface = Cast<ITDS_cppIGameActor>(myActor);
	if (myInterface)
	{
		myInterface->Execute_RemoveEffect(myActor, this);
	}

	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}

}

void UTDS_cppStateEffect::SoundSpawnByStateEffect_Multicast_Implementation(USoundBase* Sound)
{
	/*if (Sound)
	{
		UGameplayStatics::SpawnSoundAttached(Sound, myActor->GetRootComponent(), FName("ExecuteTimerSound"), FVector::ZeroVector, EAttachLocation::KeepRelativeOffset, true);
	}*/
}

void UTDS_cppStateEffect::FXSpawnByStateEffect_Multicast_Implementation(UParticleSystem* Effect, FName NameBoneHit)
{
	//if (Effect)
	//{
	//	FName NameBoneToAttached = NameBoneHit;
	//	FVector Loc = FVector(0.0f, 0.0f, 0.0f);

	//	USceneComponent* myMesh = Cast<USceneComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	//	if (myMesh)
	//	{
	//		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(Effect, myMesh, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
	//	}
	//	else
	//	{
	//		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(Effect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
	//	}

	//	// Spawn the Execute Efect Timer Sound
	//	//SoundSpawnByStateEffect_Multicast(ExecuteTimerSound);
	//	//UGameplayStatics::SpawnSoundAttached(ExecuteTimerSound, myActor->GetRootComponent(), FName("ExecuteTimerSound"), FVector::ZeroVector, EAttachLocation::KeepRelativeOffset, true);
	//}
}
 
bool UTDS_cppStateEffect_ExecuteOnce::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	ExecuteOnce();
	return true;
}

void UTDS_cppStateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTDS_cppStateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTDS_cppHealthComponent* myHealthComp = Cast<UTDS_cppHealthComponent>(myActor->GetComponentByClass(UTDS_cppHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue_OnServer(Power);
		}
		// Spawn the Execute Effect Sound 
		//SoundSpawnByStateEffect_Multicast(ExecuteEffectSound);
		//UGameplayStatics::SpawnSoundAttached(ExecuteEffectSound, myActor->GetRootComponent(), FName("ExecuteEffectSound"), FVector::ZeroVector, EAttachLocation::KeepRelativeOffset, true);
		
		//Spawn FX random array
		//int32 RndIndex = FMath::RandHelper(ExecuteParticleEffect.Num());
		//if (ExecuteParticleEffect.IsValidIndex(RndIndex) && ExecuteParticleEffect[RndIndex])
		//{
		//	FName NameBoneToAttached;
		//	FVector Loc = FVector(0.0f, 0.0f, 0.0f);

		//	//UGameplayStatics::SpawnEmitterAttached(ExecuteParticleEffect[RndIndex], myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		//	FXSpawnByStateEffect_Multicast(ExecuteParticleEffect[RndIndex], NameBoneToAttached);
		//}
	}

	DestroyObject();
}

bool UTDS_cppStateEffect_ExecuteTimer::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_cppStateEffect_ExecuteTimer::DestroyObject, Timer, false);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTDS_cppStateEffect_ExecuteTimer::Execute, RateTime, true);
	}

	//if (ParticleEffect)
	//{
	//	//to do for object with interface create func  return offset, name bones
	//	//to do random init EffectsArray
	//	FName NameBoneToAttached;
	//	FVector Loc = FVector(0.0f, 0.0f, 0.0f);
	//	
	//	ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
	//	// Spawn the Execute Efect Timer Sound
	//	UGameplayStatics::SpawnSoundAttached(ExecuteTimerSound, myActor->GetRootComponent(), FName("ExecuteTimerSound"), FVector::ZeroVector, EAttachLocation::KeepRelativeOffset, true);
	//}

		// Randomly select a particle effect from the array
		//int32 RndIndex = FMath::RandHelper(RandomParticleEffect.Num());
		/*UParticleSystem* RandomEffect = EffectsArray[RandomIndex];*/
		//if (RandomParticleEffect.IsValidIndex(RndIndex) && RandomParticleEffect[RndIndex])
		//{
			//FXSpawnByStateEffect_Multicast(RandomParticleEffect[RndIndex], NameBoneHit);
			//FName NameBoneToAttached = NameBoneHit;
			//FVector Loc = FVector(0.0f, 0.0f, 0.0f);

			//USceneComponent* myMesh = Cast<USceneComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
			//if (myMesh)
			//{
			//	ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(RandomParticleEffect[RndIndex], myMesh, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			//}
			//else
			//{
			//	ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(RandomParticleEffect[RndIndex], myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			//}

			//// Spawn the Execute Efect Timer Sound
			//UGameplayStatics::SpawnSoundAttached(ExecuteTimerSound, myActor->GetRootComponent(), FName("ExecuteTimerSound"), FVector::ZeroVector, EAttachLocation::KeepRelativeOffset, true);
		//}
		//SoundSpawnByStateEffect_Multicast(EffectSound);
	return true;
}

void UTDS_cppStateEffect_ExecuteTimer::DestroyObject()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}

	/*if(ParticleEmitter)
	{
		ParticleEmitter->DestroyComponent();
		ParticleEmitter = nullptr;
	}*/
	// Clear the effects array
	/*RandomParticleEffect.Empty();*/
	Super::DestroyObject();
}

void UTDS_cppStateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		//UGameplayStatics::ApplyDamage(myActor,Power,nullptr,nullptr,nullptr);	
		UTDS_cppHealthComponent* myHealthComp = Cast<UTDS_cppHealthComponent>(myActor->GetComponentByClass(UTDS_cppHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue_OnServer(Power);
		}
	}
}

bool UTDS_cppInvulnerabilityEffect::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	// set Invulnerability material
	ATDS_cppCharacter* myCharacter = Cast<ATDS_cppCharacter>(Actor);
	if (myCharacter)
	{
		SkeletalMeshCharacter = myCharacter->GetMesh();
		if (SkeletalMeshCharacter)
		{
			
			if (SkeletalMeshCharacter && InvulnerabilityMaterial)
			{
				SkeletalMeshCharacter->SetMaterial(0, InvulnerabilityMaterial);
			}
		}
	}
	// Set invulnerability timer
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_Invulnerability, this, &UTDS_cppInvulnerabilityEffect::OnInvulnerabilityTimer, InvulnerabilityDuration, false);
	}

	//spawn Invulnerability Effect Sound
	//SoundSpawnByStateEffect_Multicast(InvulnerabilitySound);
	//UGameplayStatics::SpawnSoundAttached(InvulnerabilitySound, myActor->GetRootComponent(), FName("InvulnerabilitySound"), FVector::ZeroVector, EAttachLocation::KeepRelativeOffset, true);

	UTDS_cppHealthComponent* myHealthComp = Cast<UTDS_cppHealthComponent>(myActor->GetComponentByClass(UTDS_cppHealthComponent::StaticClass()));
	if (myHealthComp)
	{
		myHealthComp->SetInvulnerability(true);
	}
	return true;
}

void UTDS_cppInvulnerabilityEffect::DestroyObject()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}
	//return material
	/*if (SkeletalMeshCharacter && OriginalMaterial)
	{
		SkeletalMeshCharacter->SetMaterial(0, OriginalMaterial);
	}*/
	//TO DO Original Mat
	UTDS_cppCharacterHealthComponent* CharHealthComp = Cast<UTDS_cppCharacterHealthComponent>(myActor->GetComponentByClass(UTDS_cppCharacterHealthComponent::StaticClass()));
	if (CharHealthComp)
	{
		CharHealthComp->ChangeBodyOriginalMaterial_Multicast();
	}

	Super::DestroyObject();
}


void UTDS_cppInvulnerabilityEffect::OnInvulnerabilityTimer()
{
	UTDS_cppHealthComponent* myHealthComp = Cast<UTDS_cppHealthComponent>(myActor->GetComponentByClass(UTDS_cppHealthComponent::StaticClass()));

	// Reset invulnerability 
	if (myHealthComp)
	{
		myHealthComp->SetInvulnerability(false);
	}
	DestroyObject();
}

void UTDS_cppStateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTDS_cppStateEffect, NameBone);
}

